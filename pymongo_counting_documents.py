import pymongo
from pymongo import MongoClient

# Create a connection to mongo db

# make a localhost connection explicity
myClient = MongoClient("localhost", 27017)

# Create or make a connection to a database (mydb)
db = myClient.mydb

# create a table (users)
user_table = db.users

num_users = user_table.find().count()
print("number of users: ", num_users)

fav_number = user_table.find({"favorite_number": 445}).count()
print("Favorite number elements: ", fav_number)

