import pymongo
from pymongo import MongoClient

# Create a connection to mongo db

# make a localhost connection implicity
# client = MongoClient()

# make a localhost connection explicity
myClient = MongoClient("localhost", 27017)

# Create a database (mydb)
db = myClient.mydb

# create a table (users)
users = db.users

# create a user object
user1 = {"username": "Leia",
         "password": "myverysecuredpassword",
         "favorite_number": 445,
         "hobbies": ["python",
                     "games",
                     "pizza"]}

# insert the objects that retrieve the id
user_id = users.insert_one(user1).inserted_id
print(user_id)

