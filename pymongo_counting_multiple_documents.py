import pymongo
from pymongo import MongoClient

# Create a connection to mongo db

# make a localhost connection explicity
myClient = MongoClient("localhost", 27017)

# Create or make a connection to a database (mydb)
db = myClient.mydb

# create a table (users)
user_table = db.users

# Count elemnts with a certain condition
find_elements = user_table.find({"favorite_number": 445, "username": "Leia"}).count()
print("Elements found with favorite number 445 and name leia: ", find_elements)

