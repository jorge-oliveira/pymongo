import pymongo
from pymongo import MongoClient

# Create a connection to mongo db

# make a localhost connection implicity
# client = MongoClient()

# make a localhost connection explicity
myClient = MongoClient("localhost", 27017)

# Create a database (mydb)
db = myClient.mydb

# create a table (users)
users = db.users