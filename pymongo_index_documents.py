import pymongo
from pymongo import MongoClient

# Create a connection to mongo db
myClient = MongoClient("localhost", 27017)

# Create or make a connection to a database (mydb)
db = myClient.mydb

# point the users table
Users = db.users

# create a index for table users for search performance
db.users.create_index([("username", pymongo.ASCENDING)])

# now the query its more faster
print(Users.find({"username": "nick"}))


