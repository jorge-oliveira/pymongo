import pymongo
from pymongo import MongoClient
import datetime

# Create a connection to mongo db

# make a localhost connection explicity
myClient = MongoClient("localhost", 27017)

# Create or make a connection to a database (mydb)
db = myClient.mydb

# create a table (users)
user_table = db.users

current_date = datetime.datetime.now()
print(current_date)

# create a custom date
old_date = datetime.datetime(2009, 8, 11)

uid = user_table.insert({"username": "ffie", "date": current_date})

# find and count elements date greater than ($gt)
gt = user_table.find({"date": {"$gt": old_date}}).count()
print("elements greater 2009: ", gt)

# find and count elements date greater or equal ($gte)
gte = user_table.find({"date": {"$gte": old_date}}).count()
print("elements greater or equal 2009: ", gte)

# find elements date less than ($lt)
lt = user_table.find({"date": {"$lt": old_date}}).count()
print("elements less 2009: ", lt)

# find and count elements date less or equal ($lte)
lte = user_table.find({"date": {"$lte": old_date}}).count()
print("elements less or equal 2009: ", lte)

# find and count if a field exists or not
exists = user_table.find({"date": {"$exists": True}}).count()
print("Count ", exists, " Elements with a date present.")

# find and count a field not equal
not_equal = user_table.find({"username": {"$ne": "red"}}).count()
print("Not equal: ", not_equal)

