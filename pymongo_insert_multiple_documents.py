import pymongo
from pymongo import MongoClient

# Create a connection to mongo db

# make a localhost connection implicity
# client = MongoClient()

# make a localhost connection explicity
myClient = MongoClient("localhost", 27017)

# Create a database (mydb)
db = myClient.mydb

# create a table (users)
user_table = db.users

# create a user object
users = [{"username": "Third","password": "12345"},
         {"username": "red", "password": "blue"}]

# insert the objects that retrieve the id
user_id = user_table.insert_many(users)

# get the ids of the objects inserted
print(user_id.inserted_ids)

